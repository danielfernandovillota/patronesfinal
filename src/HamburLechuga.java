public class HamburLechuga implements Hambur{

    private Hambur hambur;

    public HamburLechuga(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("Lechuga añadida a la hamburguesa");
        return "Lechuga añadida";
    }
}
