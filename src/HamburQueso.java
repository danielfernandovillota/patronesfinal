public class HamburQueso implements Hambur{

    private Hambur hambur;

    public HamburQueso(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("Queso añadido a la hamburguesa");
        return "Queso añadido";
    }
}
