public class HamburPollo implements Hambur{

    private Hambur hambur;

    public HamburPollo(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("añadir pollo a la hamburguesa");
        return "Pollo añadido";
    }
}
