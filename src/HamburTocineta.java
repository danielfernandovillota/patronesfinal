public class HamburTocineta implements Hambur{

    private Hambur hambur;

    public HamburTocineta(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("Tocineta añadida a la hamburguesa");
        return "Tocineta añadida";
    }
}
