public class HamburCarne implements Hambur{

    private Hambur hambur;

    public HamburCarne(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("carne añadida a la hamburguesa");
        return "carne añadida";
    }
}
