public class HamburArepa implements Hambur{

    private Hambur hambur;

    public HamburArepa(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("Arepa añadida a la hamburguesa");
        return "Arepa añadida";
    }
}
