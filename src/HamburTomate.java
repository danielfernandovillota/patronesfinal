public class HamburTomate implements Hambur{

    private Hambur hambur;

    public HamburTomate(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("Tomate añadido a la hamburguesa");
        return "Tomate añadido";
    }
}
