public class Carta {
    private static Carta instanciaCarta;

    private Carta(){//Se privatiza el constructor para que no se realice una instancia nueva en otro lugar

    }

    public static Carta getInstanciaCarta(){
        if (instanciaCarta==null){
            instanciaCarta=new Carta();
        }
        return instanciaCarta;

    }

   public void ConstruirHamburguesaCarne(){
       System.out.println("HAMBURGUESA CARNE");
       Base hamburBase = new Base();
       HamburSalsa hamburSalsa=new HamburSalsa(hamburBase);
       HamburCarne hamburCarne = new HamburCarne(hamburSalsa);
       HamburLechuga hamburLechuga = new HamburLechuga(hamburCarne);
       HamburTomate hamburTomate=new HamburTomate(hamburLechuga);
       HamburQueso hamburQueso=new HamburQueso(hamburTomate);
       HamburTocineta hamburTocineta=new HamburTocineta(hamburQueso);
       hamburTocineta.construirHamburguesa();
   }

    public void ConstruirHamburguesaPollo(){
        System.out.println("HAMBURGUESA Pollo");
        Base hamburBase = new Base();
        HamburSalsa hamburSalsa2=new HamburSalsa(hamburBase);
        HamburPollo hamburPollo = new HamburPollo(hamburSalsa2);
        HamburLechuga hamburLechuga = new HamburLechuga(hamburPollo);
        HamburTomate hamburTomate=new HamburTomate(hamburLechuga);
        HamburQueso hamburQueso=new HamburQueso(hamburTomate);
        HamburTocineta hamburTocineta=new HamburTocineta(hamburQueso);
        hamburTocineta.construirHamburguesa();
    }
}
