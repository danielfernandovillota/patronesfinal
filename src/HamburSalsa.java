public class HamburSalsa implements Hambur{

    private Hambur hambur;

    public HamburSalsa(Hambur hambur){
        this.hambur=hambur;
    }
    @Override
    public String construirHamburguesa() {
        this.hambur.construirHamburguesa();
        System.out.println("Salsas añadidas a la hamburguesa");
        return "Salsas añadidas";
    }
}
